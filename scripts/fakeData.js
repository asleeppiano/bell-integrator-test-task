'use strict';
exports.__esModule = true;
var fs = require('fs');
var path = require('path');
var faker_1 = require('@faker-js/faker');
var FAKE_DATA_LENGTH = 100;
var FILE_NAME = 'fakeData.json';
function makeItemsList(length) {
    return Array.from(Array(length).keys()).map(function () {
        var randomName = faker_1.faker.name.findName();
        var uuid = faker_1.faker.datatype.uuid();
        return {
            id: uuid,
            name: randomName,
        };
    });
}
function makeFakeDataList(length) {
    return Array.from(Array(length).keys()).map(function () {
        var randomName = faker_1.faker.name.findName();
        var uuid = faker_1.faker.datatype.uuid();
        var randomLength = Math.floor(Math.random() * 10);
        return {
            id: uuid,
            name: randomName,
            items: makeItemsList(randomLength),
        };
    });
}
fs.writeFileSync(
    path.resolve('public', FILE_NAME),
    JSON.stringify(makeFakeDataList(FAKE_DATA_LENGTH))
);
