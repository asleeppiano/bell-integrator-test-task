import * as fs from 'fs';
import * as path from 'path';
import { faker } from '@faker-js/faker';

type FakeItem = {
    id: string;
    name: string;
};

type FakeData = {
    items: Array<FakeItem>;
} & FakeItem;

const FAKE_DATA_LENGTH = 100;
const FILE_NAME = 'fakeData.json';

function makeItemsList(length: number): Array<FakeItem> {
    return Array.from(Array(length).keys()).map(() => {
        const randomName = faker.name.findName();
        const uuid = faker.datatype.uuid();
        return {
            id: uuid,
            name: randomName,
        };
    });
}

function makeFakeDataList(length: number): Array<FakeData> {
    return Array.from(Array(length).keys()).map(() => {
        const randomName = faker.name.findName();
        const uuid = faker.datatype.uuid();
        const randomLength = Math.floor(Math.random() * 10);
        return {
            id: uuid,
            name: randomName,
            items: makeItemsList(randomLength),
        };
    });
}

fs.writeFileSync(
    path.resolve('public', FILE_NAME),
    JSON.stringify(makeFakeDataList(FAKE_DATA_LENGTH))
);
