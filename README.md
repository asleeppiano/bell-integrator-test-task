# bell-integrator

Тестовое задание.

[Приложение](https://asleeppiano.gitlab.io/bell-integrator-test-task/)

Инициализировал проект командой:

``` sh
npm init vue@latest
```

## Стек

- typescript
- vue 3
- pinia
- vue router

## Локальный запуск

``` sh
git clone git@gitlab.com:asleeppiano/bell-integrator-test-task.git
cd bell-integrator-test-task
npm install
npm run dev
```

## Данные

Данные для списка сгенерированны с помощью `faker.js`.

Они лежат в `public.json/fakeData.json`

Их можно сгенерировать командой

``` sh
npm run makeFakeData
```
