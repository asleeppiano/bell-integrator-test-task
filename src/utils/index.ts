import type { PersonData } from '@/types';

export function debounce(
    this: unknown,
    func: (...args: any[]) => unknown,
    timeout = 300
) {
    let timer: number;
    return (...args: any[]) => {
        clearTimeout(timer);
        timer = setTimeout(() => {
            func.apply(this, args);
        }, timeout);
    };
}

export function isPersonDataType(person: unknown): person is PersonData {
    return (person as PersonData).items !== undefined;
}
