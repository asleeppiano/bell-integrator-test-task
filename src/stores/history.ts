import { OpType } from '@/const';
import type { HistoryStoreState, HistoryItem } from '@/types';
import { defineStore } from 'pinia';

export const useHistoryStore = defineStore({
    id: 'history',
    state: (): HistoryStoreState => ({
        historyList: [],
    }),
    getters: {
        addedList: (state) =>
            state.historyList.filter((item) => item.type === OpType.ADD),
        removedList: (state) =>
            state.historyList.filter((item) => item.type === OpType.REMOVE),
    },
    actions: {
        addToHistoryList(historyItem: HistoryItem) {
            this.historyList.push(historyItem);
        },
    },
});
