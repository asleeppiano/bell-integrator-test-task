import { PersonState } from '@/const';
import type { PersonsStoreState, PersonData } from '@/types';
import { defineStore } from 'pinia';
import { fetchPersonsData } from '@/services/api';
import { addDefaultStateToPersonsList } from '@/services/mapper';

export const usePersonsStore = defineStore({
    id: 'persons',
    state: (): PersonsStoreState => ({
        originalPersonList: [],
        personList: [],
        isLoading: false,
    }),
    getters: {
        selected: (state) => {
            const res = state.personList.filter(
                (person) => person.state === PersonState.SELECTED
            );
            return res;
        },
        default: (state) => {
            return state.personList.filter(
                (person) => person.state === PersonState.DEFAULT
            );
        },
    },
    actions: {
        addStateToPersonsList(personList: Array<PersonData>) {
            return personList.map((person) => {
                return {
                    ...person,
                    state: PersonState.DEFAULT,
                };
            });
        },
        async getAllPersons() {
            this.isLoading = true;
            const result = await fetchPersonsData();
            this.personList = addDefaultStateToPersonsList(result);
            this.isLoading = false;
            this.originalPersonList = this.personList;
        },
        makePersonListOriginal(): void {
            this.personList = this.originalPersonList;
        },
        changePersonState(person: PersonData, state: PersonState): void {
            this.originalPersonList = this.originalPersonList.map(
                (personItem) => {
                    if (personItem.id === person.id) {
                        return {
                            ...personItem,
                            state,
                        };
                    }
                    return personItem;
                }
            );
            this.personList = this.personList.map((personItem) => {
                if (personItem.id === person.id) {
                    return {
                        ...personItem,
                        state,
                    };
                }
                return personItem;
            });
        },
    },
});
