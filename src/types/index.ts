import type { PersonState, OpType } from '@/const';

export type Person = {
    id: string;
    name: string;
};

export type PersonResponse = {
    items: Array<Person>;
} & Person;

export type PersonData = {
    state: PersonState;
    items: Array<Person>;
} & Person;

export type MatchedPersonData = {
    state: PersonState;
    items: Array<MatchedPerson>;
    matchCount: number;
} & Person;

export type MatchedPerson = Person & { matchCount: number };

export type PersonsStoreState = {
    personList: Array<PersonData | MatchedPersonData>;
    originalPersonList: Array<PersonData>;
    isLoading: boolean;
};

export type AppRoute = {
    name: string;
    to: string;
};

export type AppRoutes = Array<AppRoute>;

type DateString = string;

export type HistoryItem = {
    type: OpType;
    date: DateString;
    person: Person;
};

export type HistoryStoreState = {
    historyList: Array<HistoryItem>;
};

export type HistoryViewMeta = {
    type: OpType;
    title: string;
};
