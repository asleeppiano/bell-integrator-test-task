export enum PersonState {
    DEFAULT,
    SELECTED,
}

export enum OpType {
    ALL = 'ALL',
    ADD = 'ADD',
    REMOVE = 'REMOVE',
}
