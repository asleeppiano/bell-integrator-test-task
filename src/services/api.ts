import type { PersonData } from '../types';

export async function fetchPersonsData(): Promise<Array<PersonData>> {
    try {
        const res = await fetch(`${import.meta.env.BASE_URL}/fakeData.json`);
        if (res.ok) {
            const json = await res.json();
            return json;
        }
        throw Error('fetch error');
    } catch {
        // i won't show any error
        return [];
    }
}
