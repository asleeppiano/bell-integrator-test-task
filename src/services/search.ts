import type {
    MatchedPersonData,
    MatchedPerson,
    PersonData,
    Person,
} from '@/types';
import { isPersonDataType } from '@/utils';

function wrapTextInColor(
    text: string,
    initPos: number,
    startPos: number,
    endPos: number
): string {
    return `${text.substring(
        initPos,
        startPos
    )}<span class="matched">${text.substring(startPos, endPos)}</span>`;
}

function matchPersonName(person: PersonData, text: string): MatchedPersonData {
    const nameMatches = person.name?.matchAll(new RegExp(text, 'g'));
    let itemsNameMatches;
    if (isPersonDataType(person)) {
        itemsNameMatches = person.items.map((item) =>
            matchPersonName(item as PersonData, text)
        );
    }
    let name = '';
    let matchCount = 0;
    let initPos = 0;
    for (const match of nameMatches) {
        if (match.index) {
            const startPos = match.index;
            const endPos = match.index + match[0].length;
            name += wrapTextInColor(
                person.name ?? '',
                initPos,
                startPos,
                endPos
            );
            initPos = endPos;
        }
        matchCount += 1;
    }
    if (initPos !== 0) {
        name += person.name?.substring(initPos);
    } else {
        name = person.name ?? '';
    }
    return {
        ...person,
        items: itemsNameMatches as Array<MatchedPerson>,
        name,
        matchCount,
    };
}

export function searchTextInList<TListItem extends PersonData | Person>(
    list: Array<TListItem>,
    text: string
): Array<MatchedPersonData> {
    return list
        .filter((listItem) => {
            return (
                listItem.name?.includes(text) ||
                (isPersonDataType(listItem) &&
                    listItem.items?.find((item) => item.name.includes(text)))
            );
        })
        .map((person) => matchPersonName(person as PersonData, text));
}
