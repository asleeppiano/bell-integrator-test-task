import type { PersonData, PersonResponse } from '@/types';
import { PersonState } from '@/const';

export function addDefaultStateToPersonsList(
    personList: Array<PersonResponse>
): Array<PersonData> {
    return personList.map((person) => {
        return {
            ...person,
            state: PersonState.DEFAULT,
        };
    });
}
